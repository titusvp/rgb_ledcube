#include "Animations.h"
#include "TLC5955.h"
#include "SPI.h"
#include "FastLED.h"

// Create TLC5955 object
TLC5955 tlc;
ANIMATION _ani;
// Pin deifnition
#define GSCLK 2
#define LAT 3
#define SPI_MOSI 13
#define SPI_CLK 12

#define L0 16// Layer 0-3
#define L1 15
#define L2  7
#define L3 11


const uint8_t TLC5955::_tlc_count = 1;          // Change to reflect number of TLC chips
float TLC5955::max_current_amps = 10;      // Maximum current output, amps
bool TLC5955::enforce_max_current = false;   // Whether to enforce max current limit

// Define dot correction, pin rgb order, and grayscale data arrays in program memory
uint8_t TLC5955::_dc_data[TLC5955::_tlc_count][TLC5955::LEDS_PER_CHIP][TLC5955::COLOR_CHANNEL_COUNT];
uint8_t TLC5955::_rgb_order[TLC5955::_tlc_count][TLC5955::LEDS_PER_CHIP][TLC5955::COLOR_CHANNEL_COUNT];
uint16_t TLC5955::_grayscale_data[TLC5955::_tlc_count][TLC5955::LEDS_PER_CHIP][TLC5955::COLOR_CHANNEL_COUNT];



void setup() {
  Serial.begin(9600);
  // The library does not ininiate SPI for you, so as to prevent issues with other SPI libraries
  SPI.begin();
  tlc.init(LAT, SPI_MOSI, SPI_CLK, GSCLK);

  // We must set dot correction values, so set them all to the brightest adjustment
  tlc.setAllDcData(127);

  // Set Max Current Values (see TLC5955 datasheet)
  tlc.setMaxCurrent(3, 3, 3); // Go up to 7

  // Set Function Control Data Latch values. See the TLC5955 Datasheet for the purpose of this latch.
  // Order: DSPRPT, TMGRST, RFRESH, ESPWM, LSDVLT
  tlc.setFunctionData(true, true, true, true, true);

  // set all brightness levels to max (127)
  int currentR = 127;
  int currentB = 127;
  int currentG = 127;
  tlc.setBrightnessCurrent(currentR, currentB, currentG);

  // Update Control Register
  tlc.updateControl();

  // Provide LED pin order (R,G,B)
  tlc.setRgbPinOrder(0, 1, 2);

  //init layerpins as output
  pinMode(L0, OUTPUT);
  digitalWrite(L0,HIGH);
  pinMode(L1, OUTPUT);
  digitalWrite(L1,HIGH);
  pinMode(L2, OUTPUT);
  digitalWrite(L2,HIGH);  
  pinMode(L3, OUTPUT);
  digitalWrite(L3,HIGH);

  
  
}//closing setup

int k,p = 0;
uint8_t layer[] = {L0,L1,L2,L3};
uint16_t fadeMask[]={0,5000,10000,15000,20000,25000,30000,35000,40000,45000,55000,60000,65000};
uint16_t fadeResolution = (bit(15)/5)-1;






  void ANIMATION::clearLeds(){
    for (int i=0; i<16; i++){ 
    tlc.setLed(i,0);
    tlc.updateLeds();
    tlc.latch();
    }
  }


  
  void ANIMATION::clearLayers(){
    digitalWrite(L0,HIGH);
    digitalWrite(L1,HIGH);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,HIGH);  
  }
  
  void ANIMATION::randomLayer(){
     digitalWrite(layer[random(0,4)],LOW);
  }
  void ANIMATION::randomRED(uint16_t bright){
     tlc.setLed(random(16),random(500, bright),0,0);
  }
    
  void ANIMATION::randomGREEN(uint16_t bright){  
    tlc.setLed(random(16),0,random(500, bright),0);
  }
    
  void ANIMATION::randomBLUE(uint16_t bright){   
    tlc.setLed(random(16),0,0,random(500, bright));
  }
                              //int 0-3         // 0-bit(15)
  void ANIMATION::setPlane(uint8_t _layer, uint16_t brightRed, uint16_t brightGreen, uint16_t brightBlue){
    for(uint8_t i=0; i<= 15; i++){
      digitalWrite(layer[_layer],LOW);
      tlc.setLed(i, brightRed, brightGreen, brightBlue);  
    }
  }

    //unfinished
  void ANIMATION::fadeSetLed(uint8_t channel, uint16_t maxBrightRed, uint16_t maxBrightGreen, uint16_t maxBrightBlue,uint16_t fadeResolution, uint8_t fadeDelay , bool invert){
    if(maxBrightRed < bit(16)&& maxBrightGreen < bit(16)&& maxBrightBlue < bit(16)&& fadeDelay > 2 && fadeResolution > 0){
      if(invert == 0){
        for(uint16_t i=0; i<= maxBrightRed/fadeResolution; i= i+fadeResolution){
          tlc.setLed(channel, maxBrightRed, maxBrightGreen, maxBrightBlue);
          tlc.updateLeds();
          tlc.latch();
          delay(fadeDelay);
        }
      }
      if(invert == 1){
        for(uint16_t i=maxBrightRed; i<= maxBrightRed/fadeResolution; i= i-fadeResolution){
          tlc.setLed(channel, maxBrightRed, maxBrightGreen, maxBrightBlue);
          tlc.updateLeds();
          tlc.latch();
          delay(fadeDelay);
        } 
      
      }
    } 
  }//closing fadeSetLed()


  void ANIMATION::randomBlink(){
    
      //delay timer between each iterate of new leds beeing turned on in ms
      const uint16_t ANIMATION_DELAY = 400;
      
      //turn on one random layer of all 4 layers
      _ani.randomLayer();
      
      // set one of 16 led towers in a random color
      tlc.setLedAppend(random(16),random(500,bit(16)-1), random(500,bit(16)-1),random(500,bit(16)-1));
      //update and turn on leds
      tlc.updateLeds();
      tlc.latch();
      
      // delay 
      delay(ANIMATION_DELAY);
      
      // if after a number of iterations all 4 layers have been turned on....
      if (digitalRead(L0)==LOW && digitalRead(L1)==LOW &&digitalRead(L2)==LOW &&digitalRead(L3)==LOW ){
        // all leds are turned off
        clearLayers();   
      }
      // if after q iterations not all layers randomly have been turned on...
      if (k == 32){
      //.. turn off all leds as well.
      clearLeds();
      // reset counter  
        k=0;
      }
      // else raise counter by 1
      k++;
  }// closing randomBlink()


  void ANIMATION::LedTestAnimation(){
    const uint16_t delayTime = 80; // dont go below 20 because of heat dissipation
    uint16_t cRed=0;
    uint16_t cGreen=0;
    uint16_t cBlue=0;
    
    for (uint8_t j=0; j<4; j++){
      if (j==0){
        digitalWrite(L0,LOW);
        cRed = bit(16)-1;
      }  
      if (j==1){
        digitalWrite(L1,LOW);
        cGreen = bit(16)-1;
      }
      if (j==2){
        digitalWrite(L2,LOW);
        cBlue = bit(16)-1;
      }
      if (j==3){
        digitalWrite(L3,LOW);
        cBlue = bit(16)-1;cGreen = 0; cRed = bit(16)-1;
      }                  
      
   
    for (uint8_t i=0;i<16;i++){
  
        //set leds on
      tlc.setLed(i,cRed, cGreen, cBlue);
      tlc.updateLeds();
      tlc.latch();
      delay(delayTime);
        //set leds off
//      tlc.setLedAppend(i,0);
//      tlc.updateLeds();
//      tlc.latch();
//      delay(delayTime);
      
       //set leds on
      tlc.setLedAppend(i,cRed, cGreen, cBlue);
      tlc.updateLeds();
      tlc.latch();
      delay(delayTime);
        //set leds off
//      tlc.setLedAppend(i,0);
//      tlc.updateLeds();
//      tlc.latch();
//      delay(delayTime);
      
        //set leds on
      tlc.setLedAppend(i,cRed, cGreen, cBlue);
      tlc.updateLeds();
      tlc.latch();
      delay(delayTime);
      //set leds off
//      tlc.setLed(i,0);
//      tlc.updateLeds();
//      tlc.latch();
//      delay(delayTime);
      tlc.setLedAppend(i,cRed, cGreen, cBlue);
      tlc.updateLeds();
      tlc.latch();
      delay(delayTime);
    }
    }
  }//LedTestAnimation closing


                                                          //    0,1,2,3
                                                          //    7,11,15,
                                                          //    14,13,12,
                                                          //    8,4,0,1
                                                          //
                                                          //    6,10,9,5
  
   void ANIMATION::spiralAnimation(){
    const uint8_t fadeResolution = 10000;
    uint8_t led_array[] = {0,1,2,3, 7,11,15, 14,13,12, 8,4,0,1,  6,10,9, 5,6,10 ,9,5,6, 10,9,4};
    //may be deleted uint16_t fadeMask[]={0,5000,10000,15000,20000,25000,30000,35000,40000,45000,55000,60000,65000};
  for(uint8_t i=0; i<=25; i++){
    if(i<=3){digitalWrite(L0,LOW), digitalWrite(L1,HIGH), digitalWrite(L2,HIGH),digitalWrite(L3,HIGH);}
    if(i> 3 && i<=6){digitalWrite(L0,HIGH), digitalWrite(L1,LOW), digitalWrite(L2,HIGH),digitalWrite(L3,HIGH);}
    if(i> 6 && i<=9){digitalWrite(L0,HIGH), digitalWrite(L1,HIGH), digitalWrite(L2,LOW),digitalWrite(L3,HIGH);}
    if(i> 9 && i<=13){digitalWrite(L0,HIGH), digitalWrite(L1,HIGH), digitalWrite(L2,HIGH),digitalWrite(L3,LOW);}
    
    if(i> 13 && i<=16){digitalWrite(L0,HIGH), digitalWrite(L1,HIGH), digitalWrite(L2,HIGH),digitalWrite(L3,LOW);}
    if(i> 16 && i<=19){digitalWrite(L0,HIGH), digitalWrite(L1,HIGH), digitalWrite(L2,LOW),digitalWrite(L3,HIGH);}
    if(i> 19 && i<=22){digitalWrite(L0,HIGH), digitalWrite(L1,LOW), digitalWrite(L2,HIGH),digitalWrite(L3,HIGH);}    
    if(i> 22 && i<=25){digitalWrite(L0,LOW), digitalWrite(L1,HIGH), digitalWrite(L2,HIGH),digitalWrite(L3,HIGH);}
    
    for(int j=0; j<12; j++ ){
     // tlc.setLed(led_array[i], fadeMask[random(12)],fadeMask[random(12)],fadeMask[random(12)]);
        tlc.setLed(led_array[i], bit(15),0,bit(15));
      }
      tlc.updateLeds();
      tlc.latch();
      
    
    delay(125);
    tlc.setAllLed(0);
    tlc.updateLeds();
    tlc.latch();
    } 
   }

    void ANIMATION::rainAnimation(){  //unfinished
  /* --------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   * xPos,yPos = allows us to select a specific LED position in a layer.
   * randomLED = is used to select the random LED from which rain falls
   * cRed, cGreen, cBlue = allows slight variations in colour of the raindrops.
   * topR, topG, topB = restores the colours of the top layer after the lightning effect
   * rainChance = affects the frequency/amount of rainfall
   * rainSpeed = the speed at which the rain falls. Recommend you set this between 2-5.
   * lightningChance = affect the frequency of lightning
   ----------------------------------------------------------------------------*/
  unsigned char xPos[16] = {0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3};
  unsigned char yPos[16] = {0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3};
  uint8_t randomLED = 0;
  uint16_t cRed[4][16];
  uint16_t cGreen[4][16];
  uint16_t cBlue[4][16];
  uint16_t topR[16];
  uint16_t topG[16];
  uint16_t topB[16];

  int rainChance;
  int rainSpeed = 5;
  int lightningChance;
   
  
  
  /*--------------------------------------------------------------------------
   * SETUP:
   * Initialise the Rainbowduino 4x4x4 RGB LED cube and the colour arrays
   --------------------------------------------------------------------------*/
 _ani.clearLeds();
  
  
  
  
  /*--------------------------------------------------------------------------
   * LOOP:
   * Randomise the chance of a lightning strike with every loop. Probability = 5%
   * Randomise the chance of a rain drop. Probability = 40%
   * Randomise the LED from which the rain drop falls from
   * Randomise the colour of the raindrop, with a bias towards blue colours
   * Record the colour of the raindrop into the topR, topG, and topB arrays.
   * Then update the display to start the raindrop animation.
   --------------------------------------------------------------------------*/

    lightningChance=random8(100);
    if(lightningChance>95){
     int chance = random8(20);
    for(int k=0; k<chance; k++){
      lightningChance=random8(100);
      if(lightningChance>random8(50)){
        for(int j=0; j<16; j++){
          chance = random8(100);
          if(chance>50){
            digitalWrite(3,LOW);
            tlc.setLed(j,255,255,255);
            tlc.updateLeds();
            tlc.latch();
          }
        }
        delay(random8(20,70));
        for(int j=0; j<16; j++){
        
          digitalWrite(3,LOW);
          tlc.setLed(j,topR[j],topG[j],topB[j]);
          tlc.updateLeds();
          tlc.latch();
        }
        delay(random8(20));
      }
      chance = random8(100);
      if(chance>70){
    for(int i=0; i<4; i++){
      for(int j=0; j<16; j++){
        
  // Illuminate the randomLED on the top layer
        if(i>2){
          if(j==randomLED){
            tlc.setLed(j, topR[j],topG[j],topB[j]);
            tlc.updateLeds();
            tlc.latch();
          }
        }
        
  // Move the raindrop down to the next level
        if(i>0){
          cRed[i-1][j] = cRed[i][j];
          cGreen[i-1][j] = cGreen[i][j];
          cBlue[i-1][j] = cBlue[i][j];
        }
  
  // Gradually fade the LEDs - Please note: the top layer is not affected
        cRed[i][j]=cRed[i][j]-50;
        cGreen[i][j]=cGreen[i][j]-40;
        cBlue[i][j]=cBlue[i][j]-40;
        if(cRed[i][j]<0){
          cRed[i][j] = 0;
        }
        if(cGreen[i][j]<0){
          cGreen[i][j] = 0;
        }
        if(cBlue[i][j]<0){
          cBlue[i][j] = 0;
        }
  
  // Update the LED brightness and rain drop animation sequence on the lower levels. 
        if(i<3){
         digitalWrite(i,LOW);
         tlc.setLed(j,cRed[i][j],cGreen[i][j],cBlue[i][j]);
         tlc.updateLeds();
         tlc.latch();
        }
        
  //Set the speed of the raindrop animation sequence
        delay(rainSpeed);
      }
    }
      }
    }
    }
  
    rainChance = random8(100);
    if(rainChance>60){
      randomLED = random8(16);
      cRed[3][randomLED]=random8(10,100);
      cGreen[3][randomLED]=random8(10,100);
      cBlue[3][randomLED]=random8(80,250);
      topR[randomLED] = cRed[3][randomLED];
      topG[randomLED] = cGreen[3][randomLED];
      topB[randomLED] = cBlue[3][randomLED];
    }
    //update Display function
    for(int i=0; i<4; i++){
      for(int j=0; j<16; j++){
        
  // Illuminate the randomLED on the top layer
        if(i>2){
          if(j==randomLED){
            tlc.setLed(j, topR[j],topG[j],topB[j]);
            tlc.updateLeds();
            tlc.latch();
          }
        }
        
  // Move the raindrop down to the next level
        if(i>0){
          cRed[i-1][j] = cRed[i][j];
          cGreen[i-1][j] = cGreen[i][j];
          cBlue[i-1][j] = cBlue[i][j];
        }
  
  // Gradually fade the LEDs - Please note: the top layer is not affected
        cRed[i][j]=cRed[i][j]-50;
        cGreen[i][j]=cGreen[i][j]-40;
        cBlue[i][j]=cBlue[i][j]-40;
        if(cRed[i][j]<0){
          cRed[i][j] = 0;
        }
        if(cGreen[i][j]<0){
          cGreen[i][j] = 0;
        }
        if(cBlue[i][j]<0){
          cBlue[i][j] = 0;
        }
  
  // Update the LED brightness and rain drop animation sequence on the lower levels. 
        if(i<3){
         digitalWrite(i,LOW);
         tlc.setLed(j,cRed[i][j],cGreen[i][j],cBlue[i][j]);
         tlc.updateLeds();
         tlc.latch();
        }
        
  //Set the speed of the raindrop animation sequence
        delay(rainSpeed);
      }
    }    

}  
  
  
  
  /*--------------------------------------------------------------------------
   * updateDisplay:
   * This function is responsible for the raindrop animation.
   * Illuminate the randomLED
   * Move the raindrop down to the next level with every cycle
   * Gradually fade the LEDs on every cycle, and prevent the values from dropping below zero
   * Update the LED brightness and rain drop animation sequence on the lower levels of the cube. 
   * The delay at the end, will determine the speed of the raindrop animation (larger values are slower)
   --------------------------------------------------------------------------*/
 /* void ANIMATION::updateDisplay(){
    
    for(int i=0; i<4; i++){
      for(int j=0; j<16; j++){
        
  // Illuminate the randomLED on the top layer
        if(i>2){
          if(j==randomLED){
            tlc.setLed(j, topR[j],topG[j],topB[j]);
          }
        }
        
  // Move the raindrop down to the next level
        if(i>0){
          cRed[i-1][j] = cRed[i][j];
          cGreen[i-1][j] = cGreen[i][j];
          cBlue[i-1][j] = cBlue[i][j];
        }
  
  // Gradually fade the LEDs - Please note: the top layer is not affected
        cRed[i][j]=cRed[i][j]-50;
        cGreen[i][j]=cGreen[i][j]-40;
        cBlue[i][j]=cBlue[i][j]-40;
        if(cRed[i][j]<0){
          cRed[i][j] = 0;
        }
        if(cGreen[i][j]<0){
          cGreen[i][j] = 0;
        }
        if(cBlue[i][j]<0){
          cBlue[i][j] = 0;
        }
  
  // Update the LED brightness and rain drop animation sequence on the lower levels. 
        if(i<3){
         digitalWrite(i,LOW);
         tlc.setLed(j,cRed[i][j],cGreen[i][j],cBlue[i][j]);
        }
        
  //Set the speed of the raindrop animation sequence
        delay(rainSpeed);
      }
    }
  }
  */
  
  
  
  /*--------------------------------------------------------------------------
   * lightning:
   * The LEDs will be set to flash a random number of times (0 to 19 times)
   * But the flash may or may not always occur.
   * If the flash does occur, then there is a 50% chance that any particular LED will join in.
   * The LED will illuminate to full brightness WHITE (255,255,255)
   * They will stay illuminated for 20 to 70 milliseconds 
   * and then return back to their normal colour for 0 to 20 milliseconds until the next flash.
   * We do not want the rain animation to stop when the lightning is flashing, so we need to update
   * the raindrop sequence from time to time. I did this randomly to eliminate any PERCEIVED delay from the
   * lightning sequence.
   * The lightning sequence and pattern is very RANDOM, which makes it very realistic.
   --------------------------------------------------------------------------*/
/*  void ANIMATION::lightning(){
    int chance = random8(20);
    for(int k=0; k<chance; k++){
      lightningChance=random8(100);
      if(lightningChance>random8(50)){
        for(int j=0; j<16; j++){
          chance = random8(100);
          if(chance>50){
            digitalWrite(3,LOW);
            tlc.setLed(j,255,255,255);
          }
        }
        delay(random8(20,70));
        for(int j=0; j<16; j++){
        
          digitalWrite(3,LOW);
          tlc.setLed(j,topR[j],topG[j],topB[j]);
        }
        delay(random8(20));
      }
      chance = random8(100);
      if(chance>70){
        updateDisplay();
      }
    }
  } */

    
    

  
